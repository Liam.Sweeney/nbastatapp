import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchNavBarComponent } from './components/search-nav-bar/search-nav-bar.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { StatTableComponent } from './components/stat-table/stat-table.component';
import { AgGridAngular } from 'ag-grid-angular'; // AG Grid Component
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SearchNavBarComponent,
    SearchBarComponent,
    NavBarComponent,
    StatTableComponent
  ],
  imports: [
    CommonModule,
    AgGridAngular,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    SearchNavBarComponent,
    StatTableComponent
  ]
})
export class SharedModule { }
