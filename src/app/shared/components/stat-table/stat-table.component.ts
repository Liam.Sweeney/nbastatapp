import { Component, Input, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community'; 
import { GridOptions } from 'src/app/player/models/grid-options';


@Component({
  selector: 'app-stat-table',
  templateUrl: './stat-table.component.html',
  styleUrls: ['./stat-table.component.css']
})
export class StatTableComponent implements OnInit{
  constructor(){}

  @Input()
  public rowData: any[] = [];

  @Input()
  public columnDefs: ColDef[] = [];

  @Input()
  public gridOptions: GridOptions = {};

  public rowHeight: number = 50;

  ngOnInit(){
  }
}
