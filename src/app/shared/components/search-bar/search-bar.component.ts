import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PlayerSeasonStatsResult } from 'src/app/player/models/player-season-stats-result';
import { PlayerService } from 'src/app/player/services/player.service';
import { Observable, concatMap, of, catchError } from 'rxjs';
import { GetPlayerSeasonStatsResponse } from 'src/app/player/models/get-player-season-stats-response';
import { NbaTeamIdEnum } from 'src/app/enums/NbaTeamIdEnum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit{
  constructor(private playerService: PlayerService,
              private router: Router
  ){

  }
  public searchForm!: FormGroup;

  season: number = 2023;
  searchQuery: string = '';
  apiPageNumber: number = 1;

  playerList: PlayerSeasonStatsResult[] = [];
  playerSearchResults: PlayerSeasonStatsResult[] = [];

  isInputFocused: boolean = false;
  currentHoveredSearchItem: any;
  isDropDownHovered: boolean = false;
  isRandomButtonEnabled: boolean = false;

  teamList: {name: string, enum: NbaTeamIdEnum}[] = [
    {name: "Atlanta Hawks", enum: NbaTeamIdEnum.AtlantaHawks},
    {name: "Boston Celtics", enum: NbaTeamIdEnum.BostonCeltics},
    {name: "Brooklyn Nets", enum: NbaTeamIdEnum.BrooklynNets},
    {name: "Charlotte Hornets", enum: NbaTeamIdEnum.CharlotteHornets},
    {name: "Chicago Bulls", enum: NbaTeamIdEnum.ChicagoBulls},
    {name: "Cleveland Cavaliers", enum: NbaTeamIdEnum.ClevelandCavaliers},
    {name: "Dallas Mavericks", enum: NbaTeamIdEnum.DallasMavericks},
    {name: "Denver Nuggets", enum: NbaTeamIdEnum.DenverNuggets},
    {name: "Detroit Pistons", enum: NbaTeamIdEnum.DetroitPistons},
    {name: "Golden State Warriors", enum: NbaTeamIdEnum.GoldenStateWarriors},
    {name: "Houston Rockets", enum: NbaTeamIdEnum.HoustonRockets},
    {name: "Indiana Pacers", enum: NbaTeamIdEnum.IndianaPacers},
    {name: "Los Angeles Clippers", enum: NbaTeamIdEnum.LosAngelesClippers},
    {name: "Los Angeles Lakers", enum: NbaTeamIdEnum.LosAngelesLakers},
    {name: "Memphis Grizzlies", enum: NbaTeamIdEnum.MemphisGrizzlies},
    {name: "Miami Heat", enum: NbaTeamIdEnum.MiamiHeat},
    {name: "Milwaukee Bucks", enum: NbaTeamIdEnum.MilwaukeeBucks},
    {name: "Minnesota Timberwolves", enum: NbaTeamIdEnum.MinnesotaTimberwolves},
    {name: "New Orleans Pelicans", enum: NbaTeamIdEnum.NewOrleansPelicans},
    {name: "New York Knicks", enum: NbaTeamIdEnum.NewYorkKnicks},
    {name: "Oklahoma City Thunder", enum: NbaTeamIdEnum.OklahomaCityThunder},
    {name: "Orlando Magic", enum: NbaTeamIdEnum.OrlandoMagic},
    {name: "Philadelphia 76ers", enum: NbaTeamIdEnum.Philadelphia76ers},
    {name: "Phoenix Suns", enum: NbaTeamIdEnum.PhoenixSuns},
    {name: "Portland Trailblazers", enum: NbaTeamIdEnum.PortlandTrailBlazers},
    {name: "Sacramento Kings", enum: NbaTeamIdEnum.SacramentoKings},
    {name: "San Antonio Spurs", enum: NbaTeamIdEnum.SanAntonioSpurs},
    {name: "Toronto Raptors", enum: NbaTeamIdEnum.TorontoRaptors},
    {name: "Utah Jazz", enum: NbaTeamIdEnum.UtahJazz},
    {name: "Washington Wizards", enum: NbaTeamIdEnum.WashingtonWizards}
  ];
  teamSearchResults: {name: string, enum: NbaTeamIdEnum}[] = [];

  ngOnInit(): void {
    this.ResetSelectForm();

    //Check local storage for AllPlayerData item
    const localStorageData = localStorage.getItem('AllPlayerData');

    if(localStorageData){
      console.log('SearchBarComponent[NgOnInit] - Item with key AllPlayerData found in local storage, assigning to playerlist')
      this.playerList = JSON.parse(localStorageData);
      this.ResetSelectForm();
    }else{
      console.log('SearchBarComponent[NgOnInit] - Item with key AllPlayerData not found in local storage, pulling from api')
      this.pullAllPlayerData().subscribe({
        next: (result: PlayerSeasonStatsResult[])=>{
          this.playerList.push(...result);
          this.ResetSelectForm();
          localStorage.setItem('AllPlayerData', JSON.stringify(this.playerList));
        },
        error: (error) => {
          console.error('SearchBarComponent[NgOnInit] - Error fetching data from PullAllPlayerData call', error);
        }
      });
    }
  }

  pullAllPlayerData(): Observable<PlayerSeasonStatsResult[]>{
    let pageNumber = 1;

    return this.pullPlayerDataRecursive(pageNumber);
  }

  pullPlayerDataRecursive(page: number): Observable<PlayerSeasonStatsResult[]>{
    return this.playerService.getAllPlayersBySeason(this.season, page).pipe(concatMap((response: GetPlayerSeasonStatsResponse) =>{
      const responseData = response.results;
      const nextUrl = response.next;

      if(nextUrl){
        return this.pullPlayerDataRecursive(page+1).pipe(
          catchError(error => of([])),
          concatMap((nextData: PlayerSeasonStatsResult[]) => of([...responseData, ...nextData]))
        );
      }else{
        return of(responseData);
      }
    }));
  }

  onInputChange(){
    this.searchQuery = this.searchForm.controls['searchInput'].value;
    this.ResetSelectForm();
    if(!this.searchQuery){
      this.teamSearchResults = [];
      this.playerSearchResults = [];
      return;
    }

    this.teamSearchResults = this.filterTeamArray(this.searchQuery);
    this.playerSearchResults = this.filterPlayerArray(this.searchQuery);
  }

  
  ResetSelectForm(){
    this.searchForm = new FormGroup( {
      searchInput: new FormControl({value: this.searchQuery, disabled: this.playerList.length > 0 ? false: true})
    });
    this.searchForm.controls['searchInput'].setValue(this.searchQuery);

    //RandomButton enabled/disabled
    if(this.playerList.length > 0){
      this.isRandomButtonEnabled = true;
    }else{
      this.isRandomButtonEnabled = false;
    }
  }

  filterPlayerArray(search: string): PlayerSeasonStatsResult[]{
    return this.playerList.filter(player => {
      return player.player_name.toLowerCase().includes(search.toLowerCase());
    })
  }
  filterTeamArray(search: string): {name: string, enum: NbaTeamIdEnum}[]{
    return this.teamList.filter(team => {
      return team.name.toLowerCase().includes(search.toLowerCase());
    })
  }

  onRandomButtonClick(){
    const playerOrTeamRandom = Math.floor(Math.random()*2)
    if(playerOrTeamRandom==0){
      //Player
      const randomPlayerIndex = Math.floor(Math.random()*this.playerList.length);
      this.onPlayerClick(this.playerList[randomPlayerIndex]);
    }else{
      //Team
      const randomTeamIndex = Math.floor(Math.random()*this.teamList.length);
      this.onTeamClick(this.teamList[randomTeamIndex]);
    }
  }

  onPlayerClick(player: PlayerSeasonStatsResult){
    this.router.navigate(['/player', player.player_name.replace(/\s+/g,'-')])
      .then(() => {
        window.location.reload();
      });
  }

  onTeamClick(team: {name: string, enum: NbaTeamIdEnum}){
    this.router.navigate(['/team', team.name.replace(/\s+/g,'-')])      
      .then(() => {
        window.location.reload();
      });;;
  }

  onSearchFocus(){
    this.isInputFocused = true;
  }

  onSearchBlur(){
    this.isInputFocused = false;
  }

  onDropdownHover(){
    this.isDropDownHovered = true;
  }

  onDropdownLeave(){
    this.isDropDownHovered = false;
  }

  onSearchItemHover(searchItem: any){
    this.currentHoveredSearchItem = searchItem;
  }
}
