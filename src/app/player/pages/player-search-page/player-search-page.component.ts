import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../services/player.service';
import { PlayerSeasonStatsResult } from '../../models/player-season-stats-result';
import { PlayerSeasonStats } from '../../models/player-season-stats';
import { ColDef } from 'ag-grid-community'; 
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-player-search-page',
  templateUrl: './player-search-page.component.html',
  styleUrls: ['./player-search-page.component.css']
})
export class PlayerSearchPageComponent implements OnInit{
  constructor(private playerService: PlayerService,
              private router: Router)
  {}

  season: number = 2023;
  rowData: PlayerSeasonStats[] = [];
  public seasonSelectForm!: FormGroup;

  colDef: ColDef[] = [
    {headerName: "Name", field: "name", headerTooltip: "Player Name"},
    {headerName: "Age", field: "age", headerTooltip: "Player Age"},
    {headerName: "Team", field: "team", headerTooltip: "Player's Team"},
    {headerName: "GP", field: "gp", headerTooltip: "Games Played"},
    {headerName: "PPG", field: "ppg", headerTooltip: "Points per game", sort: "desc", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "APG", field: "apg", headerTooltip: "Assists per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "RPG", field: "rpg", headerTooltip: "Rebounds per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "SPG", field: "spg", headerTooltip: "Steals per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "BPG", field: "bpg", headerTooltip: "Blocks per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FTPG", field: "ft", headerTooltip: "Free Throws per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TOVPG", field: "tov", headerTooltip: "Turnovers per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "PFPG", field: "pf", headerTooltip: "Personal Fouls per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FG%", field: "fg", headerTooltip: "Field Goal Percentage"},
    {headerName: "3P%", field: "threePercent", headerTooltip: "Three Point Percentage"},
    {headerName: "FT%", field: "ftPercent", headerTooltip: "Free Throw Percentage"},
  ];

  gridOptions = {
    pagination: true,
    paginationPageSize: 10,
    paginationPageSizeSelector: [10, 20, 50],
    onRowClicked: (event: any) => this.onRowClicked(event)
  };

  ngOnInit(): void {
    this.ResetSelectForm();
    this.pullPlayerData(this.season);
  }

  pullPlayerData(season: number){
    const dataKey: string = 'TopPlayerStats'+season.toString();
    const localStorageData = localStorage.getItem(dataKey);

    if(localStorageData){
      console.log(`PlayerSearchPage[pullPlayerData] - Item with key ${dataKey} found in local storage, assigning to rowData`);
      this.rowData = JSON.parse(localStorageData);
    }else{
      console.log(`PlayerSearchPage[pullPlayerData] - Item with key ${dataKey} not found in local storage, pulling from api`);
      this.playerService.getTopScoringPlayersBySeason(season).subscribe((results: PlayerSeasonStatsResult[])=>{
        if(results!==null){ 
          this.rowData = results.map(player => ({
            name: player.player_name,
            id: player.id,
            age: player.age,
            team: player.team,
            gp: player.games,
            ppg: +(player.PTS / player.games),
            apg: +(player.AST / player.games),
            rpg: +(player.TRB / player.games),
            spg: +(player.STL / player.games),
            bpg: +(player.BLK / player.games), 
            ft: +(player.ft / player.games),
            tov: +(player.TOV / player.games),
            pf: +(player.PF / player.games),
            fg: player.field_percent,
            threePercent: player.three_percent, 
            ftPercent: player.ft_percent
          }));
          localStorage.setItem(dataKey, JSON.stringify(this.rowData));
        }else{
          console.error('PlayerSearchPage[pullPlayerData] - No data found when pulling player data');
        }
        
      },
    );
    }
  }

  ResetSelectForm(){
    this.seasonSelectForm = new FormGroup( {
      seasonSelect: new FormControl({value: this.season.toString(), disabled: false})
    });
    this.seasonSelectForm.controls['seasonSelect'].setValue(this.season.toString());
  }

  UpdateSeasonSelection(){
    this.season = this.seasonSelectForm.controls['seasonSelect'].value;
    this.ResetSelectForm();
    this.pullPlayerData(this.season)
  }

  onRowClicked(event: any){
    const rowData = event.data;
    this.router.navigate(['/player', rowData.name.replace(/\s+/g,'-')])
      .then(() => {
        window.location.reload();
      });
  }
}
