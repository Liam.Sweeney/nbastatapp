import { Component, Input, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/player/services/player.service';
import { Papa, ParseResult } from 'ngx-papaparse';
import { PlayerSeasonStatsResult } from '../../models/player-season-stats-result';
import { PlayerIdResult } from '../../models/player-id-result';
import { PlayerInformationResult } from '../../models/player-information-result';
import { Observable, map } from 'rxjs';
import { PlayerSummaryStats } from '../../models/player-summary-stats';
import { ColDef } from 'ag-grid-community'; 
import { ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-player-summary-page',
  templateUrl: './player-summary-page.component.html',
  styleUrls: ['./player-summary-page.component.css']
})
export class PlayerSummaryPageComponent {
  header: boolean = true;
  constructor(private playerService: PlayerService,
              private papa: Papa,
              private activatedRoute: ActivatedRoute){}

  playerName: string =  "";
  playerThumbnailUrl: string = "";
  playerLogoId?: string = "";

  playerInformation: any; 
  parseResult: any;
  nbaHeadshotIdList: PlayerIdResult[] = [];

  rowData: PlayerSummaryStats[] = [];

  colDef: ColDef[] = [
    {headerName: "Season", field: "season", headerTooltip: "NBA Season", sort: "desc"},
    {headerName: "Age", field: "age", headerTooltip: "Player Age"},
    {headerName: "Team", field: "team", headerTooltip: "Player's Team"},
    {headerName: "GP", field: "gp", headerTooltip: "Games Played"},
    {headerName: "PPG", field: "ppg", headerTooltip: "Points per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "APG", field: "apg", headerTooltip: "Assists per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "RPG", field: "rpg", headerTooltip: "Rebounds per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "SPG", field: "spg", headerTooltip: "Steals per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "BPG", field: "bpg", headerTooltip: "Blocks per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FTPG", field: "ft", headerTooltip: "Free Throws per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TOVPG", field: "tov", headerTooltip: "Turnovers per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "PFPG", field: "pf", headerTooltip: "Personal Fouls per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FG%", field: "fg", headerTooltip: "Field Goal Percentage"},
    {headerName: "3P%", field: "threePercent", headerTooltip: "Three Point Percentage"},
    {headerName: "FT%", field: "ftPercent", headerTooltip: "Free Throw Percentage"},
  ];

  ngOnInit(): void {
    //Split the first '-' from name, for cases where name has three dashes
    this.playerName = this.activatedRoute.snapshot.paramMap.get('name')!.replace('-', ' ');
    const playerNameSplit = this.playerName.split(" ");
    const playerLastName = playerNameSplit[playerNameSplit.length-1].replace(/-/g, ' ');
    this.playerName = this.playerName.replace(/-/g, ' ');

    this.pullNbaHeadshotData();
    this.pullPlayerInformation(playerLastName);
    this.pullPlayerStats();
  }

  pullNbaHeadshotData(){
    //Pull player ids for headshots
    const NbaHeadshotLocalStorageData = localStorage.getItem('NbaHeadshotIdData');

    if(NbaHeadshotLocalStorageData){
      console.log('PlayerSummaryPage[pullNbaHeadshotData] - Item with key NbaHeadshotIdData found in local storage, assigning to nbaHeadshotIdList');
      this.nbaHeadshotIdList = JSON.parse(NbaHeadshotLocalStorageData);
      this.playerLogoId = this.nbaHeadshotIdList.find(player => player.playerName == this.playerName)?.playerId;
    }else{
      console.log('PlayerSummaryPage[pullNbaHeadshotData] - Item with key NbaHeadshotIdData not found in local storage, pulling from api');
      this.playerService.getPlayerIdData().subscribe((result: any)=>{
        if(result!==null){
          this.papa.parse(result, { header: this.header, delimiter: ',', encoding: 'utf8', complete: (result, file) => {
            this.nbaHeadshotIdList = result.data.map(({ NBAName, NBAID }: { NBAName: string; NBAID: string; }) => ({
              playerName: NBAName, 
              playerId: NBAID
            }));

              localStorage.setItem('NbaHeadshotIdData', JSON.stringify(this.nbaHeadshotIdList));
              this.playerLogoId = this.nbaHeadshotIdList.find(player => player.playerName == this.playerName)?.playerId;
              //will be used for accessing pngs: https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/{{playerId}}.png
            }});
        }else{
          console.error('PlayerSummaryPage[pullNbaHeadshotData] - No data found when pulling head shot data');
        }
      })
    }
  }

  pullPlayerInformation(playerLastName: string){
    const PlayerInformationKey = this.playerName.replace(/\s/g, '')+'PlayerInformation';
    const PlayerInformationLocalStorageData = localStorage.getItem(PlayerInformationKey);
    
    if(PlayerInformationLocalStorageData){
      console.log(`PlayerSummaryPage[pullPlayerInformation] - Item with key ${PlayerInformationKey} found in local storage, assigning to playerInformation`);
      this.playerInformation = JSON.parse(PlayerInformationLocalStorageData);
    }else{
      console.log(`PlayerSummaryPage[pullPlayerInformation] - Item with key ${PlayerInformationKey} not found in local storage, pulling from API`);
      this.playerService.getLatestPlayerInformationByName(playerLastName).subscribe(results =>{
        if(results.length!==0){
          this.playerInformation = results.find(player => {
            const fullName = `${player.firstname} ${player.lastname}`;
            return fullName === this.playerName;
          });
          localStorage.setItem(PlayerInformationKey, JSON.stringify(this.playerInformation));
        }else{
          console.error(`PlayerSummaryPage[pullPlayerInformation] - No results found for player last name: ${playerLastName} `);
        }
      });
    }
  }

  pullPlayerStats(){
    const PlayerStatsKey = this.playerName.replace(/\s/g, '')+'PlayerStats';
    const PlayerStatsLocalStorageData = localStorage.getItem(PlayerStatsKey);
    if(PlayerStatsLocalStorageData){
      console.log(`PlayerSummaryPage[pullPlayerStats] - Item with key ${PlayerStatsKey} found in local storage, assigning to rowData`);
      this.rowData = JSON.parse(PlayerStatsLocalStorageData);
    }else{
      console.log(`PlayerSummaryPage[pullPlayerStats] - Item with key ${PlayerStatsKey} not found in local storage, pulling from API`);
      this.playerService.getPlayerSeasonStatsByName(this.playerName).subscribe((results: PlayerSeasonStatsResult[])=>{
        if(results!==null){ 
          this.rowData = results.map(player => ({
            name: player.player_name,
            id: player.id,
            season: player.season,
            age: player.age,
            team: player.team,
            gp: player.games,
            ppg: +(player.PTS / player.games).toFixed(2),
            apg: +(player.AST / player.games).toFixed(2),
            rpg: +(player.TRB / player.games).toFixed(2),
            spg: +(player.STL / player.games).toFixed(2),
            bpg: +(player.BLK / player.games).toFixed(2),
            ft: +(player.ft / player.games).toFixed(2),
            tov: +(player.TOV / player.games).toFixed(2),
            pf: +(player.PF / player.games).toFixed(2),
            fg: player.field_percent,
            threePercent: player.three_percent, 
            ftPercent: player.ft_percent,
          }));
          localStorage.setItem(PlayerStatsKey, JSON.stringify(this.rowData));
        }else{
          console.error(`PlayerSummaryPage[pullPlayerStats] - No stats results found for player name: ${this.playerName} `);
        }
      });
    }
  }
}