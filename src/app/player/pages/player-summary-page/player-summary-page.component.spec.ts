import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerSummaryPageComponent } from './player-summary-page.component';

describe('PlayerSummaryPageComponent', () => {
  let component: PlayerSummaryPageComponent;
  let fixture: ComponentFixture<PlayerSummaryPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerSummaryPageComponent]
    });
    fixture = TestBed.createComponent(PlayerSummaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
