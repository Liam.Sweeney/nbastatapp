export interface PlayerSummaryStats{
    name: string,
    id: number,
    season: number;
    age: number;
    team: string;
    gp: number;
    ppg: number;
    apg: number;
    rpg: number;
    spg: number;
    bpg: number;
    ft: number;
    tov: number;
    pf: number;
    fg: string;
    threePercent: string;
    ftPercent: string;
}