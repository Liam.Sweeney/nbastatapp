import { PlayerInformationResult } from "./player-information-result"

export interface GetPlayerInformationResponse{
    get: string,
    parameters: Object,
    errors: any[],
    results: number,
    response: PlayerInformationResult[]
}