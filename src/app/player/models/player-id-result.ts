export interface PlayerIdResult{
    playerName: string,
    playerId: string,
}