export interface GridOptions{
    pagination?: boolean,
    paginationPageSize?: number
}