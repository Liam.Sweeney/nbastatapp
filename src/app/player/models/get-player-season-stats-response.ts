import { PlayerSeasonStatsResult } from "./player-season-stats-result";

export interface GetPlayerSeasonStatsResponse{
    count: number,
    next: string,
    previous: string,
    results: PlayerSeasonStatsResult[]
}