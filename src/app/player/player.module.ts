import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerSummaryPageComponent } from './pages/player-summary-page/player-summary-page.component';
import { SharedModule } from '../shared/shared.module';
import { PlayerSearchPageComponent } from './pages/player-search-page/player-search-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    PlayerSummaryPageComponent,
    PlayerSearchPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    PlayerSummaryPageComponent,
    PlayerSearchPageComponent
  ]
})
export class PlayerModule { }
