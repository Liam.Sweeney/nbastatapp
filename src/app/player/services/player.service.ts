import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Papa } from 'ngx-papaparse';
import { Observable, map, catchError, throwError } from 'rxjs';
import { ParseResult } from 'ngx-papaparse';
import { GetPlayerSeasonStatsResponse } from '../models/get-player-season-stats-response';
import { PlayerSeasonStatsResult } from '../models/player-season-stats-result';
import { RequestHeaders } from 'src/app/constants/RequestHeaders';
import { PlayerInformationResult } from '../models/player-information-result';
import { GetPlayerInformationResponse } from '../models/get-player-information-response';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient,
  ) { }

  RapidApiHeaders = {
    'X-RapidAPI-key': RequestHeaders.ApiSportsKey,
    'X-RapidAPI-Host': RequestHeaders.ApiSportsHost
  }
  RapidApiRequestOptions: Object = {
    headers: new Headers(this.RapidApiHeaders),
  };

  getPlayerIdData(): Observable<string> {
    return this.http.get('assets/NBA_Player_IDs.csv', { responseType: 'text' });
  }

  getPlayerSeasonStatsByName(playerName: string): Observable<PlayerSeasonStatsResult[]> {
    const api_url = `https://nba-stats-db.herokuapp.com/api/playerdata/name/${playerName}`;
    return this.http.get<GetPlayerSeasonStatsResponse>(api_url).pipe(
      map((apiResponse) => apiResponse.results),
      catchError(err => {
        return throwError(() => {
          console.error('PlayerService[getPlayerSeasonStatsByName] - Error Occurred')
          return err
        })})
      );
  }

  getTopScoringPlayersBySeason(season: number): Observable<PlayerSeasonStatsResult[]> {
    const api_url = `https://nba-stats-db.herokuapp.com/api/playerdata/topscorers/total/season/${season}/`;
    return this.http.get<GetPlayerSeasonStatsResponse>(api_url).pipe(
      map((apiResponse) => apiResponse.results),
      catchError(err => {
        return throwError(() => {
          console.error('PlayerService[getTopScoringPlayersBySeason] - Error Occurred')
          return err
        })})
      );
  }

  getAllPlayersBySeason(season: number, page: number): Observable<GetPlayerSeasonStatsResponse> {
    const api_url = `https://nba-stats-db.herokuapp.com/api/playerdata/season/${season}/?page=${page}`;
    return this.http.get<GetPlayerSeasonStatsResponse>(api_url).pipe(
      map((apiResponse) => apiResponse),
      catchError(err => {
        return throwError(() => {
          console.error('PlayerService[getAllPlayersBySeason] - Error Occurred')
          return err
        })})
      );
  }

  getLatestPlayerInformationByName(playerLastName: string): Observable<PlayerInformationResult[]> {
    const api_url = `https://v2.nba.api-sports.io/players?search=${playerLastName}`;
    return this.http.get<GetPlayerInformationResponse>(api_url, this.RapidApiRequestOptions).pipe(
      map((apiResponse) => apiResponse.response),
    catchError(err => {
      return throwError(() => {
        console.error('PlayerService[getLatestPlayerInformationByName] - Error Occurred')
        return err
      })})
    );
  }
}
