import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SharedModule } from '../shared/shared.module';
import { ComparePageComponent } from './pages/compare-page/compare-page.component';
import { FormsModule } from '@angular/forms';
import { CompareSelectComponent } from './components/compare-select/compare-select.component';


@NgModule({
  declarations: [
    HomePageComponent,
    ComparePageComponent,
    CompareSelectComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  exports: [
    HomePageComponent
  ]
})
export class HomeModule { }
