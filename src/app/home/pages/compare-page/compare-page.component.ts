import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PlayerSeasonStats } from 'src/app/player/models/player-season-stats';
import { NbaTeamIdEnum } from 'src/app/enums/NbaTeamIdEnum';
import { PlayerSeasonStatsResult } from 'src/app/player/models/player-season-stats-result';
import { Observable, catchError, concatMap, of } from 'rxjs';
import { PlayerService } from 'src/app/player/services/player.service';
import { CompareSelectOptions } from '../../models/compare-select-options';
import { GetPlayerSeasonStatsResponse } from 'src/app/player/models/get-player-season-stats-response';
import { TeamStandingSummary } from 'src/app/team/models/team-standing/team-standing-summary';
import { TeamService } from 'src/app/team/services/team.service';
import { PlayerIdResult } from 'src/app/player/models/player-id-result';
import { Papa } from 'ngx-papaparse';
import { TeamSummaryStats } from 'src/app/team/models/team-statistics/team-summary-stats';
import { TeamStatistics } from 'src/app/team/models/team-statistics/team-statistics';
import { PlayerSummaryStats } from 'src/app/player/models/player-summary-stats';


@Component({
  selector: 'app-compare-page',
  templateUrl: './compare-page.component.html',
  styleUrls: ['./compare-page.component.css']
})
export class ComparePageComponent implements OnInit{
  constructor(private playerService: PlayerService,
              private papa: Papa,
              private teamService: TeamService
  ){}

  nbaHeadshotIdList: PlayerIdResult[] = [];
  header: boolean = true;

  season: number = 2023;
  optionOneSelected: boolean = false;
  optionTwoSelected: boolean = false;
  optionOneSeason: number = 2023;
  optionTwoSeason: number = 2023;

  selectedPlayerOne: any;
  selectedPlayerTwo: any;
  selectedTeamOne: any;
  selectedTeamTwo: any;
  selectedTeamOneStats: any;
  selectedTeamTwoStats: any; 
  selectedTeamOneStanding: any;
  selectedTeamTwoStanding: any;

  TeamStandingsOne: TeamStandingSummary[] = [];
  TeamStandingsTwo: TeamStandingSummary[] = [];

  optionOneImgUrl: string = "";
  optionTwoImgUrl: string = "";

  teamList: {name: string, enum: NbaTeamIdEnum}[] = [
    {name: "Atlanta Hawks", enum: NbaTeamIdEnum.AtlantaHawks},
    {name: "Boston Celtics", enum: NbaTeamIdEnum.BostonCeltics},
    {name: "Brooklyn Nets", enum: NbaTeamIdEnum.BrooklynNets},
    {name: "Charlotte Hornets", enum: NbaTeamIdEnum.CharlotteHornets},
    {name: "Chicago Bulls", enum: NbaTeamIdEnum.ChicagoBulls},
    {name: "Cleveland Cavaliers", enum: NbaTeamIdEnum.ClevelandCavaliers},
    {name: "Dallas Mavericks", enum: NbaTeamIdEnum.DallasMavericks},
    {name: "Denver Nuggets", enum: NbaTeamIdEnum.DenverNuggets},
    {name: "Detroit Pistons", enum: NbaTeamIdEnum.DetroitPistons},
    {name: "Golden State Warriors", enum: NbaTeamIdEnum.GoldenStateWarriors},
    {name: "Houston Rockets", enum: NbaTeamIdEnum.HoustonRockets},
    {name: "Indiana Pacers", enum: NbaTeamIdEnum.IndianaPacers},
    {name: "Los Angeles Clippers", enum: NbaTeamIdEnum.LosAngelesClippers},
    {name: "Los Angeles Lakers", enum: NbaTeamIdEnum.LosAngelesLakers},
    {name: "Memphis Grizzlies", enum: NbaTeamIdEnum.MemphisGrizzlies},
    {name: "Miami Heat", enum: NbaTeamIdEnum.MiamiHeat},
    {name: "Milwaukee Bucks", enum: NbaTeamIdEnum.MilwaukeeBucks},
    {name: "Minnesota Timberwolves", enum: NbaTeamIdEnum.MinnesotaTimberwolves},
    {name: "New Orleans Pelicans", enum: NbaTeamIdEnum.NewOrleansPelicans},
    {name: "New York Knicks", enum: NbaTeamIdEnum.NewYorkKnicks},
    {name: "Oklahoma City Thunder", enum: NbaTeamIdEnum.OklahomaCityThunder},
    {name: "Orlando Magic", enum: NbaTeamIdEnum.OrlandoMagic},
    {name: "Philadelphia 76ers", enum: NbaTeamIdEnum.Philadelphia76ers},
    {name: "Phoenix Suns", enum: NbaTeamIdEnum.PhoenixSuns},
    {name: "Portland Trailblazers", enum: NbaTeamIdEnum.PortlandTrailBlazers},
    {name: "Sacramento Kings", enum: NbaTeamIdEnum.SacramentoKings},
    {name: "San Antonio Spurs", enum: NbaTeamIdEnum.SanAntonioSpurs},
    {name: "Toronto Raptors", enum: NbaTeamIdEnum.TorontoRaptors},
    {name: "Utah Jazz", enum: NbaTeamIdEnum.UtahJazz},
    {name: "Washington Wizards", enum: NbaTeamIdEnum.WashingtonWizards}
  ];

  playerList: PlayerSeasonStatsResult[] = [];
  selectOptionsData: CompareSelectOptions[] = [];
  playerOptions: CompareSelectOptions[] = [];
  teamOptions: CompareSelectOptions[] = [];
  defaultOptionText: string = 'Players'
  selectedRadioOption: string = 'player'

  ngOnInit(): void {
    //Check local storage for AllPlayerData item
    const localStorageData = localStorage.getItem('AllPlayerData');

    if(localStorageData){
      console.log('ComparePageComponent[NgOnInit] - Item with key AllPlayerData found in local storage, assigning to playerlist')
      this.playerList = JSON.parse(localStorageData);

      //Map player options for passing to the select
      this.playerOptions = this.playerList.map(player => ({name: player.player_name, id: player.id}));
      //sort playerOptions alphabetically
      this.playerOptions = this.playerOptions.sort(function(a, b) {
        var textA = a.name.toUpperCase();
        var textB = b.name.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
      });
    }else{
      console.log('ComparePageComponent[NgOnInit] - Item with key AllPlayerData not found in local storage, pulling from api')
      this.pullAllPlayerData().subscribe({
        next: (result: PlayerSeasonStatsResult[])=>{
          this.playerList.push(...result);

          localStorage.setItem('AllPlayerData', JSON.stringify(this.playerList));
          this.playerOptions = this.playerList.map(player => ({name: player.player_name, id: player.id}));
          //sort playerOptions alphabetically
          this.playerOptions = this.playerOptions.sort(function(a, b) {
            var textA = a.name.toUpperCase();
            var textB = b.name.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
        },
        error: (error) => {
          console.error('ComparePageComponent[NgOnInit] - Error fetching data from PullAllPlayerData call', error);
        }
      });
    }

    //Pull Nba player headshot data
    this.pullNbaHeadshotData();

    //pull teamOptions to pass to select
    this.teamOptions = this.teamList.map(team => ({name: team.name, id: team.enum}));
    //sort teamOptions alphabetically
    this.teamOptions = this.teamOptions
    this.teamOptions = this.teamOptions.sort(function(a, b) {
      var textA = a.name.toUpperCase();
      var textB = b.name.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    this.selectOptionsData = this.playerOptions;
  }

  //Pulls all player data, calls recursive function
  pullAllPlayerData(): Observable<PlayerSeasonStatsResult[]>{
    let pageNumber = 1;

    return this.pullPlayerDataRecursive(pageNumber);
  }

  //Recursively iterates through pages of allplayerdata, combines into one observable
  pullPlayerDataRecursive(page: number): Observable<PlayerSeasonStatsResult[]>{
    return this.playerService.getAllPlayersBySeason(this.season, page).pipe(concatMap((response: GetPlayerSeasonStatsResponse) =>{
      const responseData = response.results;
      const nextUrl = response.next;

      if(nextUrl){
        return this.pullPlayerDataRecursive(page+1).pipe(
          catchError(error => of([])),
          concatMap((nextData: PlayerSeasonStatsResult[]) => of([...responseData, ...nextData]))
        );
      }else{
        return of(responseData);
      }
    }));
  }

  pullNbaHeadshotData(){
    //Pull player ids for headshots
    const NbaHeadshotLocalStorageData = localStorage.getItem('NbaHeadshotIdData');

    if(NbaHeadshotLocalStorageData){
      console.log('ComparePageComponent[pullNbaHeadshotData] - Item with key NbaHeadshotIdData found in local storage, assigning to nbaHeadshotIdList');
      this.nbaHeadshotIdList = JSON.parse(NbaHeadshotLocalStorageData);
    }else{
      console.log('ComparePageComponent[pullNbaHeadshotData] - Item with key NbaHeadshotIdData not found in local storage, pulling from api');
      this.playerService.getPlayerIdData().subscribe((result: any)=>{
        if(result!==null){
          this.papa.parse(result, { header: this.header, delimiter: ',', encoding: 'utf8', complete: (result, file) => {
            this.nbaHeadshotIdList = result.data.map(({ NBAName, NBAID }: { NBAName: string; NBAID: string; }) => ({
              playerName: NBAName, 
              playerId: NBAID
            }));

              localStorage.setItem('NbaHeadshotIdData', JSON.stringify(this.nbaHeadshotIdList));
              //will be used for accessing pngs: https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/{{playerId}}.png
            }});
        }else{
          console.error('ComparePageComponent[pullNbaHeadshotData] - No data found when pulling head shot data');
        }
      })
    }
  }

  //Pull team stats, assign to proper selectionvariable
  pullTeamSummaryData(teamName: string, teamId: number, season:number, selectNumber: number){
    //Check local storage to see if the relevant season stats already exists
    const TeamSummaryStatsKey = teamName.replace(/\s/g, '')+season.toString()+'SummaryStats';
    const TeamSummaryLocalStorageData = localStorage.getItem(TeamSummaryStatsKey);

    if(TeamSummaryLocalStorageData){
      //Pull data from local storage
      console.log(`TeamSummaryPageComponent[pullTeamSummaryData] - Item with key ${TeamSummaryStatsKey} found in local storage, assigning to rowData`)
      if(selectNumber==1){
        this.selectedTeamOneStats = JSON.parse(TeamSummaryLocalStorageData)[0];
      }else{
        this.selectedTeamTwoStats = JSON.parse(TeamSummaryLocalStorageData)[0];
      }
    }else{
      //Pull from api
      console.log(`TeamSummaryPageComponent[pullTeamSummaryData] - Item with key ${TeamSummaryStatsKey} not found in local storage, pulling from API`)
      this.teamService.getTeamStatsByTeamIdAndSeason(teamId, season).subscribe((results: TeamStatistics[])=>{
        if(results!==null){ 
          const resultData = results.map(teamStats => ({
            games: teamStats.games,
            fbp: teamStats.fastBreakPoints/teamStats.games,
            pip: teamStats.pointsInPaint/teamStats.games,
            scp: teamStats.secondChancePoints/teamStats.games,
            pot: teamStats.pointsOffTurnovers/teamStats.games,
            ppg: teamStats.points/teamStats.games,
            fgm: teamStats.fgm/teamStats.games,
            fga: teamStats.fga/teamStats.games,
            fgp: teamStats.fgp,
            ftm: teamStats.ftm/teamStats.games,
            fta: teamStats.fta/teamStats.games,
            ftp: teamStats.ftp,
            tpm: teamStats.tpm/teamStats.games,
            tpa: teamStats.tpa/teamStats.games,
            tpp: teamStats.tpp,
            orpg: teamStats.offReb/teamStats.games,
            drpg: teamStats.defReb/teamStats.games,
            trpg: teamStats.totReb/teamStats.games,
            apg: teamStats.assists/teamStats.games,
            pf: teamStats.pFouls/teamStats.games,
            spg: teamStats.steals/teamStats.games,
            tov: teamStats.turnovers/teamStats.games,
            bpg: teamStats.blocks/teamStats.games,
            plusMinus: teamStats.plusMinus
          }));
          if(selectNumber==1){
            this.selectedTeamOneStats = resultData[0];

          }else{
            this.selectedTeamTwoStats = resultData[0];
          }
          localStorage.setItem(TeamSummaryStatsKey, JSON.stringify(resultData));
        }else{
          console.error('TeamSummaryPageComponent[pullTeamSummaryData] - Error pulling team summary data, no data found')
        }
      });
    }
  }

  //Pull team standings, assing to proper variables, each selection has their own team stats and standing
  pullTeamStandings(season: number, selectNumber: number){
    //Check if standings exist in localstorage already
    const TeamStandingsKey = 'AllTeamStandings'+season.toString();
    const AllTeamStandingsLocalStorageData = localStorage.getItem(TeamStandingsKey);

    if(AllTeamStandingsLocalStorageData){
      //Data exists in local storage, assign to proper selection standing variables
      if(selectNumber===1){
        console.log(`ComparePageComponent[pullTeamStandings] - Item with key ${TeamStandingsKey} found in local storage, assigning to optionOne standings`)
        this.TeamStandingsOne = JSON.parse(AllTeamStandingsLocalStorageData);
      }else{
        console.log(`ComparePageComponent[pullTeamStandings] - Item with key ${TeamStandingsKey} found in local storage, assigning to optionTwo standings`)
        this.TeamStandingsTwo = JSON.parse(AllTeamStandingsLocalStorageData);
      }
    }else{
      console.log(`ComparePageComponent[pullTeamStandings] - Item with key ${TeamStandingsKey} not found in local storage, pulling from API`)
      this.teamService.getTeamStandingsBySeason(season).subscribe((results) => {
        if(results){
          const standings = results.map(standing => ({
            conference: standing.conference.name,
            conferenceRank: standing.conference.rank,
            teamName: standing.team.name,
            division: standing.division.name,
            divisionRank: standing.division.rank,
            wins: standing.win.total,
            losses: standing.loss.total,
            winrate: standing.win.percentage
        }));
  
        if(selectNumber===1){
          this.TeamStandingsOne = standings;
        }else{
          this.TeamStandingsTwo = standings;
        }
        localStorage.setItem(TeamStandingsKey, JSON.stringify(standings));
        }else{
          console.error('ComparePageComponent[pullTeamStandings] - Error pulling team standings, no data found');
        }
      });
    }  
  }

  //Pull stats, transform to per game stats, assign to either PlayerOption one or two
  pullPlayerSummaryStatsByName(playerName: string, season: number, selectedNumber: number){
    //Remove spaces for naming localstorage item with playername
    const PlayerStatsKey = playerName.replace(/\s/g, '')+'PlayerStats';
    const PlayerStatsLocalStorageData = localStorage.getItem(PlayerStatsKey);

    //Check whether item exists in localstorage
    if(PlayerStatsLocalStorageData){
      //Pull Local Storage data
      console.log(`PlayerSummaryPage[pullPlayerStats] - Item with key ${PlayerStatsKey} found in local storage, assigning to rowData`);
      const playerStats: PlayerSummaryStats[] = JSON.parse(PlayerStatsLocalStorageData);

      //Search stats array for item matching season
      const playerSummaryStatSearchResult = playerStats.filter(player=>player.season === season);

      //Assign data to proper variable for selection
      if(selectedNumber === 1){
        this.selectedPlayerOne = playerSummaryStatSearchResult[0];
      }else{
        this.selectedPlayerTwo = playerSummaryStatSearchResult[0];
      }
    }else{
      //Pull data from api and transform to TeamSummaryStats
      console.log(`PlayerSummaryPage[pullPlayerStats] - Item with key ${PlayerStatsKey} not found in local storage, pulling from API`);
      this.playerService.getPlayerSeasonStatsByName(playerName).subscribe((results: PlayerSeasonStatsResult[])=>{
        if(results!==null){ 
          const playerStats: PlayerSummaryStats[] = results.map(player => ({
            name: player.player_name,
            id: player.id,
            season: player.season,
            age: player.age,
            team: player.team,
            gp: player.games,
            ppg: +(player.PTS / player.games).toFixed(2),
            apg: +(player.AST / player.games).toFixed(2),
            rpg: +(player.TRB / player.games).toFixed(2),
            spg: +(player.STL / player.games).toFixed(2),
            bpg: +(player.BLK / player.games).toFixed(2),
            ft: +(player.ft / player.games).toFixed(2),
            tov: +(player.TOV / player.games).toFixed(2),
            pf: +(player.PF / player.games).toFixed(2),
            fg: player.field_percent,
            threePercent: player.three_percent, 
            ftPercent: player.ft_percent,
          }));
          localStorage.setItem(PlayerStatsKey, JSON.stringify(playerStats));

          //Search stats array, pull stats for this season, assign to player selection
          const playerSummaryStatSearchResult = playerStats.filter(player=>player.season === season);
          if(selectedNumber === 1){
            this.selectedPlayerOne = playerSummaryStatSearchResult[0];
          }else{
            this.selectedPlayerTwo = playerSummaryStatSearchResult[0];
          }
        }else{
          console.error('ComparePageComponent[pullPlayerSummaryStatsByName] - Error pulling player summary stats, no data found')
        }
      });
    }
  }

  onRadioOptionChange(option: string){
    this.selectedRadioOption = option;
    
    //Reset variables when switching selections
    if(option==='player'){
      this.optionOneSelected=false;
      this.optionTwoSelected=false;
      this.selectedTeamOne = null;
      this.selectedTeamTwo = null;
      this.selectedTeamOneStats = null;
      this.selectedTeamTwoStats = null;
      this.selectedTeamOneStanding = null;
      this.selectedTeamTwoStanding = null;
      this.optionOneImgUrl = "";
      this.optionTwoImgUrl = "";
      this.defaultOptionText="Players";
      this.selectOptionsData = this.playerOptions;
    }else{
      this.optionOneSelected=false;
      this.optionTwoSelected=false;
      this.selectedPlayerOne = null;
      this.selectedPlayerTwo = null;
      this.optionOneImgUrl = "";
      this.optionTwoImgUrl = "";
      this.defaultOptionText="Teams";
      this.selectOptionsData = this.teamOptions;
    }
  }

  //Handle changes in child select element
  onCompareSelectChange(event: any){
    if(this.selectedRadioOption ==='player'){
    //Player option is selected
      if(event.value == ''){
        //Default option was selected, reset variables
        if(event.selectNumber==1){
          //Reset option one variables
          this.selectedPlayerOne = null;
          this.optionOneSelected = false;
          this.optionOneSeason = 2023;
        }else if(event.selectNumber==2){
          //Reset option two variables
          this.selectedPlayerTwo = null;
          this.optionTwoSelected = false;
          this.optionTwoSeason = 2023;
        }
      }else{
        //Non-default option selected
        const playerSummaryStatSearchResult = this.playerList.filter(player=>player.id === +event.value);
        if(event.selectNumber==1){
          //Player option one, populate player stats and image
          const logoId = this.nbaHeadshotIdList.find(player => player.playerName == playerSummaryStatSearchResult[0].player_name)?.playerId;
          this.optionOneImgUrl = `https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/${logoId}.png`
          this.optionOneSelected = true;
          this.pullPlayerSummaryStatsByName(playerSummaryStatSearchResult[0].player_name, this.optionOneSeason, 1);
        }else if(event.selectNumber==2){
          //Player option two, populate player stats and image
          const logoId = this.nbaHeadshotIdList.find(player => player.playerName == playerSummaryStatSearchResult[0].player_name)?.playerId;
          this.optionTwoImgUrl = `https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/${logoId}.png`
          this.optionTwoSelected = true;
          this.pullPlayerSummaryStatsByName(playerSummaryStatSearchResult[0].player_name, this.optionTwoSeason, 2);
        }
      }
    }else{
    //Team option is selected
      if(event.value == ''){
        // Default blank option selected, reset variables
        if(event.selectNumber==1){
          //Reset option one variables
          this.selectedTeamTwo = null;
          this.selectedTeamOneStats = null;
          this.selectedTeamOneStanding = null;
          this.optionOneSelected = false;
          this.optionOneSeason = 2023;
        }else if(event.selectNumber==2){
          //Reset option two variables
          this.selectedTeamTwo = null;
          this.selectedTeamTwoStats = null;
          this.selectedTeamTwoStanding = null;
          this.optionTwoSelected = false;
          this.optionTwoSeason = 2023;
        }
      }else{
        //non default team select option selected
        const teamSearchResult = this.teamList.filter(team=>team.enum === +event.value)
        if(event.selectNumber==1){
          //populate option one team with stats and img
          this.selectedTeamOne = teamSearchResult[0];
          this.optionOneSelected = true;
          this.optionOneImgUrl = `assets/teamLogos/team-${this.selectedTeamOne.enum}.png`;
          this.pullTeamSummaryData(this.selectedTeamOne.name, this.selectedTeamOne.enum, this.optionOneSeason, 1);
          this.pullTeamStandings(this.optionOneSeason, 1);
          //Pull this teams standing from total team standing, use array index since this function returns an array
          let teamName = ""
          //Replace clippers name since it abbreviates LA in the standing results
          if(this.selectedTeamOne.name === "Los Angeles Clippers"){
            teamName = "LA Clippers";
          }
          else{
            teamName = this.selectedTeamOne.name;
          }
          this.selectedTeamOneStanding = this.TeamStandingsOne.filter(team=>team.teamName === teamName)[0];


        }else if(event.selectNumber==2){
          //populate option two team with stats and img
          this.selectedTeamTwo = teamSearchResult[0];
          this.optionTwoSelected = true;
          this.optionTwoImgUrl = `assets/teamLogos/team-${this.selectedTeamTwo.enum}.png`;
          this.pullTeamSummaryData(this.selectedTeamTwo.name, this.selectedTeamTwo.enum, this.optionTwoSeason, 2);
          this.pullTeamStandings(this.optionTwoSeason, 2);
          //Pull this teams standing from total team standing
          let teamName = ""
          //Replace clippers name since it abbreviates LA in the standing results
          if(this.selectedTeamTwo.name === "Los Angeles Clippers"){
            teamName = "LA Clippers";
          }
          else{
            teamName = this.selectedTeamTwo.name;
          }
          this.selectedTeamTwoStanding = this.TeamStandingsTwo.filter(team=>team.teamName === teamName)[0];
        }
      }
    }
  }
  
  UpdateOptionOneSeasonSelection(event: any){
    const value: string = event.target.value;
    this.optionOneSeason = +value;
    if(this.selectedRadioOption==='player'){
      //Pull relevant season player stats on season change
      this.pullPlayerSummaryStatsByName(this.selectedPlayerOne.name, this.optionOneSeason, 1);
    }else{
      //Pull team summary stats and standings for this season
      this.pullTeamSummaryData(this.selectedTeamOne.name, this.selectedTeamOne.enum, this.optionOneSeason, 1);
      this.pullTeamStandings(this.optionOneSeason, 1);
      //Pull this teams standing from total team standing
      let teamName = ""
      //Replace clippers name since it abbreviates LA in the standing results
      if(this.selectedTeamOne.name === "Los Angeles Clippers"){
        teamName = "LA Clippers";
      }
      else{
        teamName = this.selectedTeamOne.name;
      }
      this.selectedTeamOneStanding = this.TeamStandingsOne.filter(team=>team.teamName === teamName)[0];
    }
  }

  isHigherStat(statValueOne?: number, statValueTwo?: number): boolean{
    if(statValueOne == null){
      return false;
    }
    if(statValueTwo == null){
      return false;
    }
    return statValueOne > statValueTwo;
  }

  isLowerStat(statValueOne?: number, statValueTwo?: number): boolean{
    if(statValueOne == null){
      return false;
    }
    if(statValueTwo == null){
      return false;
    }
    return statValueOne < statValueTwo;
  }

  UpdateOptionTwoSeasonSelection(event: any){
    const value: string = event.target.value;
    this.optionTwoSeason = +value;
    if(this.selectedRadioOption==='player'){
      //Pull relevant season player stats on season change
      this.pullPlayerSummaryStatsByName(this.selectedPlayerTwo.name, this.optionTwoSeason, 2);
    }else{
      //Pull team summary stats and standings for this season
      this.pullTeamSummaryData(this.selectedTeamTwo.name, this.selectedTeamTwo.enum, this.optionTwoSeason, 2);
      this.pullTeamStandings(this.optionTwoSeason, 2);
      //Pull this teams standing from total team standing
      let teamName = ""
      //Replace clippers name since it abbreviates LA in the standing results
      if(this.selectedTeamTwo.name === "Los Angeles Clippers"){
        teamName = "LA Clippers";
      }
      else{
        teamName = this.selectedTeamTwo.name;
      }
      this.selectedTeamTwoStanding = this.TeamStandingsTwo.filter(team=>team.teamName === teamName)[0];
    }
  }
}
