import { Component, Input, Output, OnInit } from '@angular/core';
import { CompareSelectOptions } from '../../models/compare-select-options';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-compare-select',
  templateUrl: './compare-select.component.html',
  styleUrls: ['./compare-select.component.css']
})
export class CompareSelectComponent implements OnInit{
  constructor(){}

  @Input() defaultOptionText: string = "";
  @Input() selectOptionData: CompareSelectOptions[] = [];
  @Input() selectNumber: number = 0;
  @Output() selectChangeEmitter: EventEmitter<{value: number, selectNumber: number}> = new EventEmitter<{value: number, selectNumber: number}>();

  ngOnInit(): void {
  }

  emitOnChangeEvent(event: any){
    const eventData: number = event.target.value;
    this.selectChangeEmitter.emit({value: eventData, selectNumber: this.selectNumber});
  }
}
