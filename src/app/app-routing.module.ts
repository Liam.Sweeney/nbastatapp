import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home/pages/home-page/home-page.component';
import { PlayerSearchPageComponent } from './player/pages/player-search-page/player-search-page.component';
import { TeamSearchPageComponent } from './team/pages/team-search-page/team-search-page.component';
import { PlayerSummaryPageComponent } from './player/pages/player-summary-page/player-summary-page.component';
import { TeamSummaryPageComponent } from './team/pages/team-summary-page/team-summary-page.component';
import { ComparePageComponent } from './home/pages/compare-page/compare-page.component';

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: 'full'},
  { path: "home", component: HomePageComponent },
  { path: "players", component: PlayerSearchPageComponent },
  { path: "teams", component: TeamSearchPageComponent },
  { path: "player/:name", component: PlayerSummaryPageComponent },
  { path: "team/:name", component: TeamSummaryPageComponent},
  { path: "compare", component: ComparePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
