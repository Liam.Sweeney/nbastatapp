import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamSummaryPageComponent } from './pages/team-summary-page/team-summary-page.component';
import { TeamSearchPageComponent } from './pages/team-search-page/team-search-page.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TeamSummaryPageComponent,
    TeamSearchPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    TeamSummaryPageComponent,
    TeamSearchPageComponent,
  ]
})
export class TeamModule { }
