import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSearchPageComponent } from './team-search-page.component';

describe('TeamSearchPageComponent', () => {
  let component: TeamSearchPageComponent;
  let fixture: ComponentFixture<TeamSearchPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamSearchPageComponent]
    });
    fixture = TestBed.createComponent(TeamSearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
