import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ColDef } from 'ag-grid-community'; 
import { TeamStandingSummary } from '../../models/team-standing/team-standing-summary';
import { TeamService } from '../../services/team.service';
import { TeamStanding } from '../../models/team-standing/team-standing';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-search-page',
  templateUrl: './team-search-page.component.html',
  styleUrls: ['./team-search-page.component.css']
})
export class TeamSearchPageComponent implements OnInit{
  constructor(private teamService: TeamService,
              private router: Router
  ){}

  season: number = 2023;
  rowDataEast: TeamStandingSummary[] = [];
  rowDataWest: TeamStandingSummary[] = [];

  public seasonSelectForm!: FormGroup;

  colDef: ColDef[] = [
    {headerName: "Rank", field: "conferenceRank", headerTooltip: "Conference Rank", flex: 1 ,sort: "asc"},
    {headerName: "Team Name", field: "teamName", headerTooltip: "Team Name", flex: 2},
    {headerName: "Wins", field: "wins", headerTooltip: "Team Wins", flex: 1},
    {headerName: "Losses", field: "losses", headerTooltip: "Team Losses", flex: 1},
    {headerName: "Winrate", field: "winrate", headerTooltip: "Team Winrate", flex: 1},
  ]

  gridOptions = {
    pagination: true,
    paginationPageSize: 5,
    paginationPageSizeSelector: [5, 10, 15],
    onRowClicked: (event: any) => this.onRowClicked(event)
  };

  ngOnInit(): void {
    this.ResetSelectForm();
    this.pullTeamStandings();
  }

  pullTeamStandings(){
    const EastTeamStandingsKey = 'EastTeamStandings'+this.season.toString();
    const WestTeamStandingsKey = 'WestTeamStandings'+this.season.toString();
    const EastTeamStandingsLocalStorageData = localStorage.getItem(EastTeamStandingsKey);
    const WestTeamStandingsLocalStorageData = localStorage.getItem(WestTeamStandingsKey);

    if(EastTeamStandingsLocalStorageData&&WestTeamStandingsLocalStorageData){
      console.log(`TeamSearchPageComponent[pullTeamStandings] - Items with keys ${EastTeamStandingsKey} and ${WestTeamStandingsKey} found in local storage, assigning to standings`)
      this.rowDataEast = JSON.parse(EastTeamStandingsLocalStorageData);
      this.rowDataWest = JSON.parse(WestTeamStandingsLocalStorageData);
    }else{
      console.log(`TeamSearchPageComponent[pullTeamStandings] - Items with keys ${EastTeamStandingsKey} and ${WestTeamStandingsKey} not found in local storage, pulling from API`)
      this.teamService.getTeamStandingsBySeason(this.season).subscribe((results) => {
        if(results){
          const { easternConferenceStandings, westernConferenceStandings } = results.reduce((accumulator, standing) => {
            const standingSummary: TeamStandingSummary = {
              conference: standing.conference.name,
              conferenceRank: standing.conference.rank,
              teamName: standing.team.name,
              division: standing.division.name,
              divisionRank: standing.division.rank,
              wins: standing.win.total,
              losses: standing.loss.total,
              winrate: standing.win.percentage
            }
    
            if (standing.conference.name === 'east') {
              accumulator.easternConferenceStandings.push(standingSummary);
            } else if (standing.conference.name === 'west') {
              accumulator.westernConferenceStandings.push(standingSummary);
            }
            return accumulator;
          }, { easternConferenceStandings: [] as TeamStandingSummary[], westernConferenceStandings: [] as TeamStandingSummary[] });
    
          this.rowDataEast = easternConferenceStandings;
          this.rowDataWest = westernConferenceStandings;
          localStorage.setItem(EastTeamStandingsKey, JSON.stringify(this.rowDataEast));
          localStorage.setItem(WestTeamStandingsKey, JSON.stringify(this.rowDataWest));
        }else{
          console.error(`TeamSearchPageComponent[ngOnInit] - No team standing data found for season: ${this.season} `);
        }
      });
    }  
  }

  onRowClicked(event: any){
    const rowData = event.data;
    this.router.navigate(['/team', rowData.teamName.replace(/\s+/g,'-')])
      .then(() => {
        window.location.reload();
      });
  }

  ResetSelectForm(){
    this.seasonSelectForm = new FormGroup( {
      seasonSelect: new FormControl({value: this.season.toString(), disabled: false})
    });
    this.seasonSelectForm.controls['seasonSelect'].setValue(this.season.toString());
  }

  UpdateSeasonSelection(){
    this.season = this.seasonSelectForm.controls['seasonSelect'].value;
    this.ResetSelectForm();
    this.pullTeamStandings();
  }
}
