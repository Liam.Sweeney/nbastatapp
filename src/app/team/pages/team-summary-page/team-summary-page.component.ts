import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../services/team.service';
import { NbaTeamIdEnum } from 'src/app/enums/NbaTeamIdEnum';
import { TeamSummaryStats } from '../../models/team-statistics/team-summary-stats';
import { TeamStatistics } from '../../models/team-statistics/team-statistics';
import { ColDef } from 'ag-grid-community'; 
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { JsonPipe } from '@angular/common';


@Component({
  selector: 'app-team-summary-page',
  templateUrl: './team-summary-page.component.html',
  styleUrls: ['./team-summary-page.component.css']
})
export class TeamSummaryPageComponent implements OnInit{
  constructor(private teamService: TeamService,
              private activatedRoute: ActivatedRoute){}

  team: NbaTeamIdEnum = NbaTeamIdEnum.Default;

  teamName: string = "";
  teamInfo: any;
  season: number = 2023;
  rowData: TeamSummaryStats[] = [];

  public seasonSelectForm!: FormGroup;

  colDef: ColDef[] = [
    {headerName: "Games", field: "games", headerTooltip: "Games Played"},
    {headerName: "PPG", field: "ppg", headerTooltip: "Points per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "APG", field: "apg", headerTooltip: "Assists per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "ORPG", field: "orpg", headerTooltip: "Offensive Rebounds per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "DRPG", field: "drpg", headerTooltip: "Defensive Rebounds per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TRPG", field: "trpg", headerTooltip: "Total Rebounds per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "SPG", field: "spg", headerTooltip: "Steals per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "BPG", field: "bpg", headerTooltip: "Blocks per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TOV", field: "tov", headerTooltip: "Turnovers per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "PF", field: "pf", headerTooltip: "Personal Fouls per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FGM", field: "fgm", headerTooltip: "Field Goals Made per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FGA", field: "fga", headerTooltip: "Field Goals Attempted per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FGP", field: "fgp", headerTooltip: "Field Goal Percentage average"},
    {headerName: "FTM", field: "ftm", headerTooltip: "Free Throws Made per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FTA", field: "fta", headerTooltip: "Free Throws Attempted per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "FTP", field: "ftp", headerTooltip: "Free Throw Percentage average"},
    {headerName: "TPM", field: "tpm", headerTooltip: "Three Points Made per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TPA", field: "tpa", headerTooltip: "Three Points Attempted per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "TPP", field: "tpp", headerTooltip: "Three Point Percentage average"},
    {headerName: "FBP", field: "fbp", headerTooltip: "Fastbreak Points per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "PIP", field: "pip", headerTooltip: "Points in the Paint per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "SCP", field: "scp", headerTooltip: "Second Chance Points per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "POT", field: "pot", headerTooltip: "Points Off Turnovers per game", valueFormatter: params => Number(params.value).toFixed(2)},
    {headerName: "+/-", field: "plusMinus", headerTooltip: "Total Plus Minus"},
  ];

  ngOnInit(): void {
    this.teamName = this.activatedRoute.snapshot.paramMap.get('name')!.replace(/-/g,'');
    this.team = NbaTeamIdEnum[this.teamName as keyof typeof NbaTeamIdEnum];

    const TeamInformationKey = this.teamName+'Information';
    const TeamInformationLocalStorageData = localStorage.getItem(TeamInformationKey);

    if(TeamInformationLocalStorageData){
      console.log(`TeamSummaryPageComponent[NgOnInit] - Item with key ${TeamInformationKey} found in local storage, assigning to teamInfo`)
      this.teamInfo = JSON.parse(TeamInformationLocalStorageData);
    }else{
      console.log(`TeamSummaryPageComponent[NgOnInit] - Item with key ${TeamInformationKey} not found in local storage, pulling from API`)
      this.teamService.getTeamInformationByTeamId(this.team).subscribe(results => {
        if(results){
          this.teamInfo = results[0];
          localStorage.setItem(TeamInformationKey, JSON.stringify(this.teamInfo));
        }else{
          console.error(`TeamSummaryPageComponent[ngOnInit] - No team information data found for team id: ${this.team} `);
        }
      });
    }
    this.ResetSelectForm();
    this.pullTeamSummaryData();
  }

  pullTeamSummaryData(){
    const TeamSummaryStatsKey = this.teamName+this.season.toString()+'SummaryStats';
    const TeamSummaryLocalStorageData = localStorage.getItem(TeamSummaryStatsKey);

    if(TeamSummaryLocalStorageData){
      console.log(`TeamSummaryPageComponent[pullTeamSummaryData] - Item with key ${TeamSummaryStatsKey} found in local storage, assigning to rowData`)
      this.rowData = JSON.parse(TeamSummaryLocalStorageData);
    }else{
      console.log(`TeamSummaryPageComponent[pullTeamSummaryData] - Item with key ${TeamSummaryStatsKey} not found in local storage, pulling from API`)
      this.teamService.getTeamStatsByTeamIdAndSeason(this.team, this.season).subscribe((results: TeamStatistics[])=>{
        if(results!==null){ 
          this.rowData = results.map(teamStats => ({
            games: teamStats.games,
            fbp: teamStats.fastBreakPoints/teamStats.games,
            pip: teamStats.pointsInPaint/teamStats.games,
            scp: teamStats.secondChancePoints/teamStats.games,
            pot: teamStats.pointsOffTurnovers/teamStats.games,
            ppg: teamStats.points/teamStats.games,
            fgm: teamStats.fgm/teamStats.games,
            fga: teamStats.fga/teamStats.games,
            fgp: teamStats.fgp,
            ftm: teamStats.ftm/teamStats.games,
            fta: teamStats.fta/teamStats.games,
            ftp: teamStats.ftp,
            tpm: teamStats.tpm/teamStats.games,
            tpa: teamStats.tpa/teamStats.games,
            tpp: teamStats.tpp,
            orpg: teamStats.offReb/teamStats.games,
            drpg: teamStats.defReb/teamStats.games,
            trpg: teamStats.totReb/teamStats.games,
            apg: teamStats.assists/teamStats.games,
            pf: teamStats.pFouls/teamStats.games,
            spg: teamStats.steals/teamStats.games,
            tov: teamStats.turnovers/teamStats.games,
            bpg: teamStats.blocks/teamStats.games,
            plusMinus: teamStats.plusMinus
          }));
          localStorage.setItem(TeamSummaryStatsKey, JSON.stringify(this.rowData));
        }else{
          console.error(`TeamSummaryPageComponent[pullTeamSummaryData] - No team summary data found for team id: ${this.team} and season: ${this.season} `);
        }
      });
    }
  }

  ResetSelectForm(){
    this.seasonSelectForm = new FormGroup( {
      seasonSelect: new FormControl({value: this.season.toString(), disabled: false})
    });
    this.seasonSelectForm.controls['seasonSelect'].setValue(this.season.toString());
  }

  UpdateSeasonSelection(){
    this.season = this.seasonSelectForm.controls['seasonSelect'].value;
    this.ResetSelectForm();
    this.pullTeamSummaryData();
  }
}
