import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSummaryPageComponent } from './team-summary-page.component';

describe('TeamSummaryPageComponent', () => {
  let component: TeamSummaryPageComponent;
  let fixture: ComponentFixture<TeamSummaryPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamSummaryPageComponent]
    });
    fixture = TestBed.createComponent(TeamSummaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
