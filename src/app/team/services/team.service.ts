import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TeamStatistics } from '../models/team-statistics/team-statistics';
import { GetTeamStatisticsResponse } from '../models/team-statistics/get-team-statistics-response';
import { Observable, tap, map } from 'rxjs';
import { catchError, throwError } from 'rxjs';
import { RequestHeaders } from '../../constants/RequestHeaders';
import { GetTeamStandingsResponse } from '../models/team-standing/get-team-standings-response';
import { TeamStanding } from '../models/team-standing/team-standing';
import { GetTeamInformationResponse } from '../models/get-team-information-response';
import { TeamInformation } from '../models/team-information';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  headers ={
    'X-RapidAPI-key':  RequestHeaders.xRapidApiKey,
    'X-RapidAPI-Host': RequestHeaders.xRapidApiHost
  }
  requestOptions: Object = {
    headers: new Headers(this.headers),
  };

  constructor(private http: HttpClient) { }

  getTeamStatsByTeamIdAndSeason(teamId: number, season: number): Observable<TeamStatistics[]>{
    const api_url = `https://api-nba-v1.p.rapidapi.com/teams/statistics?id=${teamId}&season=${season}`;
    return this.http.get<GetTeamStatisticsResponse>(api_url, this.requestOptions).pipe(
      map((apiResponse) => apiResponse.response),
      catchError(err => {
        return throwError(() => {
          console.error('TeamService[getTeamStatsByTeamIdAndSeason] - Error Occurred')
          return err
        })})
      );
  }

  getTeamStandingsBySeason(season: number): Observable<TeamStanding[]>{
    const api_url = `https://api-nba-v1.p.rapidapi.com/standings?league=standard&season=${season}`;
    return this.http.get<GetTeamStandingsResponse>(api_url, this.requestOptions).pipe(
      map((apiResponse) => apiResponse.response),
      catchError(err => {
        return throwError(() => {
          console.error('TeamService[getTeamStandingsBySeason] - Error Occurred')
          return err
        })})
      );
  }
  
  getTeamInformationByTeamId(teamId: number): Observable<TeamInformation[]>{
    const api_url = `https://api-nba-v1.p.rapidapi.com/teams?id=${teamId}`;
    return this.http.get<GetTeamInformationResponse>(api_url, this.requestOptions).pipe(
      map((apiResponse) => apiResponse.response),
      catchError(err => {
        return throwError(() => {
          console.error('TeamService[getTeamInformationByTeamId] - Error Occurred')
          return err
        })})
      );
  }
}
