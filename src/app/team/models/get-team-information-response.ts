import { TeamInformation } from "./team-information"

export interface GetTeamInformationResponse{
    get: string,
    parameters: object,
    errors: any[],
    results: number,
    response: TeamInformation[]
}