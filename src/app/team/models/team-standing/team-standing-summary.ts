export interface TeamStandingSummary{
    conference: string,
    conferenceRank: number,
    teamName: string,
    division: string,
    divisionRank: number,
    wins: number,
    losses: number,
    winrate: string
}