import { TeamStanding } from "./team-standing";

export interface GetTeamStandingsResponse{
    get: string,
    parameters: Object,
    errors: any[],
    results: number,
    response: TeamStanding[]
}