export interface TeamStandingWinInformation{
    home: number,
    away: number,
    total: number,
    percentage: string,
    lastTen: number
}