import { TeamStatistics } from "./team-statistics";

export interface GetTeamStatisticsResponse{
    get: string,
    parameters: Object,
    errors: any[],
    results: number,
    response: TeamStatistics[]
}